package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Adapter extends ArrayAdapter<String> {
        CountryList country_list;
        public Adapter(Context context, String[] countries) {
            super(context, 0, countries);
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            String countryname =(String) getItem(position);
            if (view == null) {
                view = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            }
            final Country country = country_list.getCountry(countryname);
            ImageView image = (ImageView) view.findViewById(R.id.imageC);
            TextView countryNmae = (TextView) view.findViewById(R.id.countryN);
            int id = view.getResources().getIdentifier("com.example.myapplication:drawable/" +country.getmImgFile() , null, null);
            image.setImageResource(id);
            countryNmae.setText(countryname);
            return view;
        }


}
