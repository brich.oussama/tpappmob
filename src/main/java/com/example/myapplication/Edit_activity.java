package com.example.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Edit_activity extends AppCompatActivity {
    CountryList listContry;

    String countrySelectedName;
    Country countrySelected;
    ImageView image;
    TextView contryName;
    EditText capital;
    EditText langue;
    EditText monnaie;
    EditText population;
    EditText superficie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        listContry = new CountryList();
        //Relie les variables et le design
        image = (ImageView) findViewById(R.id.imageView);
        contryName = (TextView) findViewById(R.id.contryName);
        capital = (EditText) findViewById(R.id.capitalEdit);
        langue = (EditText) findViewById(R.id.langueEdit);
        monnaie = (EditText) findViewById(R.id.monnaieEdit);
        population = (EditText) findViewById(R.id.populationEdit);
        superficie = (EditText) findViewById(R.id.superEditt);

        //recuperer le pays selectionné
        Intent intent = getIntent();
        countrySelectedName = intent.getStringExtra("countryChoice");
        countrySelected  = listContry.getCountry(countrySelectedName);

        //recuperer l'image du pays
        int id = getResources().getIdentifier("com.example.myapplication:drawable/" + countrySelected.getmImgFile().toString(), null, null);
        image.setImageResource(id);

        //recuperer les donnes de ce pays
        contryName.setText(countrySelectedName.toString());
        capital.setText(countrySelected.getmCapital().toString());
        langue.setText(countrySelected.getmLanguage().toString());
        monnaie.setText(countrySelected.getmCurrency().toString());
        population.setText(String.valueOf(countrySelected.getmPopulation()));
        superficie.setText(String.valueOf(countrySelected.getmArea())+" Km²");

        //botton save
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrySelected.setmCapital(capital.getText().toString());
                countrySelected.setmLanguage(langue.getText().toString());
                countrySelected.setmCurrency(monnaie.getText().toString());
                countrySelected.setmPopulation(Integer.valueOf(population.getText().toString()));
                //eleminate km² from the area String
                String arr[] = superficie.getText().toString().split(" ", 2);
                countrySelected.setmArea(Integer.valueOf(arr[0].toString()));
                listContry.setCountry(countrySelectedName,countrySelected);
                Toast.makeText(getApplicationContext(),"la modification a bien été effectuée ",Toast.LENGTH_LONG).show();


            }
        });


    }
}
