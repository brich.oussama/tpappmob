package com.example.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private String[] pays;
    CountryList  pays_list;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pays=pays_list.getNameArray();
        listView=(ListView) findViewById(R.id.listPrincipale);

       /* final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1, pays);*/
        final Adapter adapter = new Adapter(this,pays);
        listView.setAdapter(adapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                Intent myIntent = new Intent(v.getContext(),Edit_activity.class);
                String contryChoice=(String)(listView.getItemAtPosition(position));
                myIntent.putExtra("countryChoice",contryChoice);
                startActivity(myIntent);


            }
        });



    }

}

